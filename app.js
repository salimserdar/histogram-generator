const arr = [1,2,3,4,5,6,7,8,9,10,10,9,8,7,6,5,4,3,2,1];
const histogram = document.getElementById('histogram');

/**
 * Generas a histogram
 * 
 * @param {array} arr 
 */
const genrateHistogram = (arr, min = 0, max = 10) => {
    let range = max - min;
    for(let i = 0; i < arr.length; i++) {
        let bar = document.createElement('div');
        bar.classList.add('histogram__item');
        bar.style.height = (arr[i] - min) / range * 100 + '%';
        bar.style.backgroundColor = getRandomColor();
        histogram.appendChild(bar);
    }
}

genrateHistogram(arr);

/**
 * Generate random color
 * 
 * @returns {string}
 */
function getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }